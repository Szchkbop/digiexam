var answers = require('./answers.json');
var questions = require('./exam.json');
var helper = require('./digiTestHelper');

/* ------------------------------------------------ */
/* --------------------Main "Loop"----------------- */
/* ------------------------------------------------ */

	totalScore = 0;
	for (n=0; n < answers.answers.length; n++) {
		console.log(questions.questions[n].title + ' given answer was ' + answers.answers[n].alternativesIndex + '.')
		tmp = helper.assignPoints(answers.answers[n].alternativesIndex, questions.questions[n].correctAlternativesIndex, questions.questions[n].questionType);
		totalScore += tmp;
		if(helper.compareArrays(answers.answers[n].alternativesIndex, questions.questions[n].correctAlternativesIndex) === true) {
			console.log('Correct! Assigned ' + tmp + ' points!\n');
		}
		else {
			console.log('Incorrect! The correct answer was ' + questions.questions[n].correctAlternativesIndex + '. Assigned ' + tmp + ' points!\n');
		}
	}
	console.log('Total score ' + totalScore);


