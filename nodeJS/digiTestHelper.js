/*
Used by the main loop to show wether or not the two answer indices match perfectly.
*/
exports.compareArrays = function(givenAnswers, correctAnswers) {
	if (givenAnswers.length !== correctAnswers.length) {
		return false;
	}
	for (i=0; i < givenAnswers.length ;i++) {
		if (givenAnswers[i] !== correctAnswers[i]) {
			return false;
		}
	}
return true;
}

/*
Used by assignPoints() to check multiple choice questions. Returns the specified points for each answer (target) in correctAnswers.
*/
exports.isIn = function(target, correctAnswers) {
	for (i=0; i < correctAnswers.length; i++) {
		if (target === correctAnswers[i]){
			return 1;
		}
	}
return -1;
}

/*
Used by the main loop.
Calculates and returns the score for a single question.
*/
exports.assignPoints = function(givenAnswers, correctAnswers, questionType) {
	if (questionType === 'singleChoice') {
		if (givenAnswers[0] == correctAnswers[0]) {
			return 3
		}
		else {
			return 0
		}
	}
	else if (questionType === 'multipleChoice') {
		score = 0;
		for (c=0; c < (givenAnswers.length); c++) {
			score += exports.isIn(givenAnswers[c], correctAnswers);
		}
		if (score >= 0) {
			return score;
		}
	}

	else { //Probable faulty input data. Do nothing to fix the problem, simply notify user.
		console.log("Unknown question type\n");
	}
return 0;
}
