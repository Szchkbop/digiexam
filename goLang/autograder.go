package main

import (
	"fmt"
	"encoding/json"
	"io/ioutil"
)

/* ------------------------------------------------ */
/* ---------------------Structs-------------------- */
/* ------------------------------------------------ */
//Used to catch the exam questions from file.
type DigiTestQuestions struct {
	Title string `json:"title"`
	Questions []qst `json:questions"`
}

//Used to access exam questions.
type qst struct {
	Title string `json:"title"`
	QuestionType string `json:"questionType"`
	Alternatives []int `json:"alternatives"`
	CorrectIndices []int `json:"correctAlternativesIndex"`
}

//Used to catch the exam answers from file.
type DigiTestAnswers struct {
	Answers []ans `json:"answers"`
}

//Used to access exam questions.
type ans struct {
	AnswerIndices []int `json:"alternativesIndex"`
}

/* ------------------------------------------------ */
/* -------------------Main "loop------------------- */
/* ------------------------------------------------ */

func main () {
	questions := unpackQuestions("exam.json")

	answers := unpackAnswers("answers.json")
	
	gradeAnswers(answers, questions)
}

/* ------------------------------------------------ */
/* -------------------JSON Encoding---------------- */
/* ------------------------------------------------ */
//Used to "gracefully" exit if file read fails.
func check (e error) {
	if e != nil {
		panic(e)
	}
}

//Extracts JSON data from file into exam questions format.
func unpackQuestions(fileName string) []qst {
	examData, err := ioutil.ReadFile(fileName)
		check(err)
	
	var questions DigiTestQuestions
	json.Unmarshal(examData, &questions)
return questions.Questions
}

//Extracts JSON data from file into exam answers format.
func unpackAnswers(fileName string) []ans {
	answerData, err := ioutil.ReadFile(fileName)
		check(err)

	var answers DigiTestAnswers
	json.Unmarshal(answerData, &answers)
return answers.Answers
}


/* ------------------------------------------------ */
/* ------------------Grading methods--------------- */
/* ------------------------------------------------ */
//HELPER METHOD, used by assignPoints()
//Returns points earned per answer given on multiple choice questions.
func findInSlice (target int, slice []int) int {
	for i := range(slice) {
		if target == slice[i] {
			return 1
		}
	}
return -1
}

//HELPER METHOD, used by gradeAnswers()
//Returns the ammount of points earned on a single exam question.
func assignPoints (givenAnswer, correctAnswer []int, questionType string) int{
	var score int
	switch questionType {
	case "singleChoice":
		if givenAnswer[0] == correctAnswer[0] {
			return 3
		} else {
			return 0
		}
	case "multipleChoice":
		for i := range(givenAnswer) {
			score += findInSlice(givenAnswer[i], correctAnswer)
		}
		if score <= 0 {
			return 0
		} else {
			return score
		}
	default:
		panic("Unknown question type\n")
	}

}

//HELPER METHOD
//Assesses wether an answer was fully correct and displays results.
func compareSlices(givenAnswer, correctAnswer []int) {
	if len(givenAnswer) != len(correctAnswer) {
		fmt.Printf("Incorrect!, the correct answer was %v.\n", correctAnswer)
		return
	}
	for i := range(givenAnswer) {
		if givenAnswer[i] != correctAnswer[i] {
			fmt.Printf("Incorrect!, the correct answer was %v.\n", correctAnswer)
			return
		}
	}
	fmt.Printf("Correct!\n")
return
}

//CONVENTION: Assumed both arguments are sorted to correspond to eachother
//Main grading method, contains hardcoded rulesets for grading.
func gradeAnswers (answer []ans, question []qst){
	totalScore := 0
	for i := range(question) {
		tmp := assignPoints(answer[i].AnswerIndices, question[i].CorrectIndices, question[i].QuestionType)
		totalScore += tmp
	
		fmt.Printf("%s, you answered %v\n", question[i].Title, answer[i].AnswerIndices)
		compareSlices(answer[i].AnswerIndices, question[i].CorrectIndices)
		fmt.Printf("%d points awarded!\n\n", tmp)
	}
	fmt.Printf("Total score %d.\n", totalScore)
return
}


